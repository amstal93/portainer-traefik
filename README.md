# Portainer & Traefik

in order to get valid certificates for my homelab i use traefik with aws name resolver (since my domian is hosted in route53).

the configuration of traefik is very simple. it works with labes attached to other containers running. the ui itself just shows the current status and since everything is only visible inside of my network i do not have any issue with exposing the read-only traefik-ui to my internal website.

# .env

i store my enviroment varialbes inside a .env file so i can reuse the same docker-compose on my local computer as well as on my local homelab or my homelab that runs in the cloud. with only minior adjustments.

my current .env looks like this
````ini
AWS_HOSTED_ZONE_ID=xxx
AWS_ACCESS_KEY_ID=xxx
AWS_SECRET_ACCESS_KEY=xxx
TRAEFIK_URL=traefik.dev-pc.home.slohr.de
PORTAINER_URL=portainer.dev-pc.home.slohr.de
````

these access keys are to an IAM user which is used to validate my local certificates and has following IAM Policy attached to it [source](https://go-acme.github.io/lego/dns/route53/)
````json
{
   "Version": "2012-10-17",
   "Statement": [
       {
           "Sid": "",
           "Effect": "Allow",
           "Action": [
               "route53:GetChange",
               "route53:ChangeResourceRecordSets",
               "route53:ListResourceRecordSets"
           ],
           "Resource": [
               "arn:aws:route53:::hostedzone/*",
               "arn:aws:route53:::change/*"
           ]
       },
       {
           "Sid": "",
           "Effect": "Allow",
           "Action": "route53:ListHostedZonesByName",
           "Resource": "*"
       }
   ]
}
````